/**
 * Author: Andrzej Turko
 * Date: 2020-20-20
 * License: CC0
 * Description:
 * Time:
 */
const int N = 1e5+4; // max. length of word + 4
// Ranks of indices in rnk, sorted indices in SA
// lcp[i] = LCP of i-th and (i+1)-th suffix (in lex. order)

namespace suff {
	int rnk[N], SA[N], Txt[N], lcp[N];
	void radix (int n, int *k, int *a, int *b, int K) {
		int *c = new int [K];
		rep(i,0,K) c[i]=0;
		rep(i,0,n) c[k[a[i]]]++;
		int suma=0;
		rep(i,0,K) {int t = c[i]; c[i] = suma; suma +=t; }
		rep(i,0,n) b[c[k[a[i]]]++] = a[i];
		delete[] c;
	}
	bool cmp (int a, int b, int *T) {
		while (1) {
			if (T[a]!=T[b]) return T[a] < T[b];
			if (a%3!=0 && b%3!=0) return rnk[a] < rnk[b];
			a++; b++; 
		}
	}
	void suffArr (int *T, int *A, int n, int K) {
		T[n] = T[n+1] = T[n+2] = T[n+3] = 0;
		n++;
		int n0 = (n+2)/3, n1 = (n+1)/3, n2 = n/3;
		int Temp[n], W0[n0], W[n], k, c, d;
		rep(i,0,n) Temp[i]=i;
		radix(n, T+2, Temp, A, K);
		radix(n, T+1, A, Temp, K);
		radix(n, T, Temp, A, K);
		
		Temp[A[0]] = k = 1;
		rep(i,1,n) {
			k+= (T[A[i]]!=T[A[i-1]] || T[A[i]+1]!= T[A[i-1]+1] || T[A[i]+2]!=T[A[i-1]+2]);
			Temp[A[i]]=k;
		}
		
		rep(i,1,n) if (Temp[A[i]]==Temp[A[i-1]]) {
			c = d = 0;
			rep(a,0,n) if (a%3==0) W0[d++] = a;
			rep(a,0,n) if (a%3==1) T[c++] = Temp[a];
			rep(a,0,n) if (a%3==2) T[c++] = Temp[a];
			suffArr(T, A, c, k+1);
			rep(j,0,c) {
				if (A[j]<n1) A[j] = A[j] * 3 +1;
				else A[j] = (A[j] - n1) * 3 +2;
			}
			rep(j,0,c) rnk[A[j]] = j + 1;
			rnk[n] = 0;
			radix(n0, rnk+1, W0, W, n+1);
			radix(n0, Temp, W, W0, k+1);
			
			c = d = 0;
			while (d < n0 && c < n1 + n2) {
				if (cmp (W0[d], A[c], Temp)) {
					W[c+d]=W0[d];
					d++;
				} else {
					W[c+d] = A[c];
					c++;
				}
			}
			while (d < n0) {W[c+d] = W0[d]; d++;}
			while (c < n1 + n2) {W[c+d] = A[c]; c++;}
			rep(j,0,n) A[j] = W[j];
			break;
		}
		rep(i,1,n) A[i-1] = A[i];
	}
	void main (string s, int K='z') {
		int n = s.size();
		rep(i,0,n) Txt[i] = s[i];
		suffArr(Txt, SA, n, K+1);
		rep(i,0,n) rnk[SA[i]] = i;
		
		rep(i,0,n-1) lcp[i] = 1;
		int p = 0; s.pb('#');
		rep(i,0,n) {
			if (rnk[i]==n-1) {
				p = 0;
				continue;
			} 
			while (s[i+p] == s[SA[rnk[i]+1]+p]) p++;
			lcp[rnk[i]] = p;
			if (p>0) p--;
		}
	}
};
