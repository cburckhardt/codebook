/**
 * Author: Andrzej Turko
 * Date: 2020-20-20
 * License: CC0
 * Description:
 * Time:
 */
//aho corasick
//lowercase english letters
//ends of occurances in res vector : <id, where>

struct aho {
	vector <vector <int> > graf;
	vector <int> up, word, rob, znak, found;
	vector <pair <int, int> > depth;
	aho() {
		rob.resize(27, -1);
		graf.pb(rob);
		up.pb(-1);
		found.pb(-1);
		word.pb(-1);
		znak.pb('#');
	}
	int suf (int a, int b) {
		while (a>=0) {
			if (graf[a][b]!=-1) return graf[a][b];
			a = up[a];
		}
		return 0;
	}
	void add (string &s, int id, int a=0)  {
		rep(pos,0,(int)s.size()) {
			if (graf[a][s[pos]-'a']==-1) {
				graf[a][s[pos]-'a']=(int)up.size();
				depth.pb(mp(pos, (int)up.size()));
				up.pb(a);
				znak.pb(s[pos]-'a');
				word.pb(-1);
				found.pb(-1);
				graf.pb(rob);
			}
			a = graf[a][s[pos]-'a'];
		}
		word[a] = id;
	}
	//preprocessing
	void ups () {
		sort(depth.begin(), depth.end());
		for (pair <int, int> p: depth) {
			up[p.se]=suf(up[up[p.se]], znak[p.se]);
			found[p.se] = word[up[p.se]]>=0 ? up[p.se] : found[up[p.se]];
		}
	}
	//pattern search
	int walk (string &s, vector <pair <int,int> > &res, int a=0) {
		rep(pos,0,(int)s.size()) {
			while (a>0 && graf[a][s[pos]-'a']<0) a = up[a];
			a = max(0,graf[a][s[pos]-'a']);
			int b=a;
			do {if (word[b]>=0) res.pb(mp(word[b],pos));} 
			while ((b = found[b])>=0);
		}
		return (int)res.size();
	}
};
