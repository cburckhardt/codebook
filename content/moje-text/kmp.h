/**
 * Author: Andrzej Turko
 * Date: 2020-20-20
 * License: CC0
 * Description:
 * Time:
 */

namespace kmp //zwraca poczatki wystapien
{
	vector <int> prefsuf;
	int n, m;
	//modyfikuje stringi
	vector <int> main (string s, string t) //s - slowo \\ t- wzorzec -
	{
		prefsuf.clear();
		n=t.size(); //wzor
		m=s.size(); //tekst
		
		t="0"+t;
		prefsuf.resize(n+1, 0);
		prefsuf[0]=-1; //bo nie na nawet litery
		int x; //dlugosc preiksu if success
		rep(i,2,n+1)
		{
			x=prefsuf[i-1]+1;
			while (t[x]!=t[i] && x>0)
			{
				x=prefsuf[x-1]+1; //dlugosc prefiksu, ktory robie
			}
			prefsuf[i]=x;
		}
		//tablica gotowa
		vector <int> occ;
		int p=0; //position to be
		rep(i,0,m)
		{
			p++; //czy mozemy pojsc dalej
			while (s[i]!=t[p] && p>0)
			{
				p=prefsuf[p-1]+1;
			}
			
			if (p==n) 
			{
				occ.pb(i-n+1); //poczatek slowa
				p=prefsuf[p];
			}
		}
		return occ;
	}
