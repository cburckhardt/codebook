/**
 * Author: Andrzej Turko
 * Date: 2020-20-20
 * License: CC0
 * Description:
 * Time:
 */
typedef ll T;
const T inf = 1e9;
const int N = 200;
//the c table will be destroyed
namespace hungarian {
	T c[N][N]; //fill weights here
	int match[N], n; //result is here match[i] (left) matched with i (right)
	bitset <N> free, AL, AR;
	bool dfs (int a) {
		AL[a] = true;
		rep(i,0,n) if (c[a][i]==0) if (match[i]==-1 || (!AL[match[i]] && dfs(match[i]))) {
			match[i] = a;
			return true;
		}
		return false;
	}
	T matching (int n_) {
		n = n_;
		int m = 0;
		T ret = 0;
		free.set();
		rep(i,0,n) match[i] = -1;
		while(m<n) {
			bool f= true;
			while (f) {
				AL.reset();
				f = false;
				rep(i,0,n) if (free[i] && dfs(i)) {
					free[i] = false;
					m++;
					f = true;
				}
			}
			if (m==n) break;
			rep(i,0,n) AR[i] =  (match[i]!=-1 && AL[match[i]]);
			T d = inf;
			rep(i,0,n) rep(j,0,n) if (AL[i] && !AR[j]) d = min(d, c[i][j]);
			
			rep(i,0,n) rep(j,0,n) {
				if (AL[i] && !AR[j]) c[i][j]-=d;
				if (!AL[i] && AR[j]) c[i][j]+=d;
			}
			rep(i,0,n) {
				if (AL[i]) ret+=d;
				if (AR[i]) ret-=d;
			}
		}
		return ret;
	}
};

