/**
 * Author: Andrzej Turko
 * Date: 2020-20-20
 * License: CC0
 * Description:
 * Time:
 */
// Min Cost Max Flow implementation using dijkstra
// 0-indexed vertices
struct MCMF {
	static const int N = 10000, M = 400000; //vertices, 2*edges+2
	typedef int T1; //capacity type
	typedef int T2; //cost type
	int To[M], Nxt[M], E[N], Vst[N], q, n, cnt = 1;
	pair <T2, int> H[M];
	T1 Cap[M];
	const T1 inf = 1e8;
	T2 Cost[M], D[N];
	
	void init(int _n) {
		n = _n;
		q = 1;
		rep(i,0,n) E[i] = 0;
	}
	
	void addEdge(int from, int to, T1 cap, T2 cost) {
		Nxt[++q] = E[from];
		E[from] = q;
		To[q] = to;
		Cap[q] = cap;
		Cost[q] = cost;
		
		Nxt[++q] = E[to];
		E[to] = q;
		To[q] = from;
		Cap[q] = 0;
		Cost[q] = -cost;
	}
	
	bool dijkstra (int s, int t, T2 &dist) {
		
		fill(D, D + n, inf);
		D[s] = 0;
		H[0] = {0,s};
		for (int k=1; k>0; pop_heap(H, H+k)) {
			int v = H[--k].se;
			for (int e=E[v]; e>0; e=Nxt[e]) if(Cap[e]>0 and D[To[e]] > D[v] + Cost[e]) {
				H[k++] = {D[To[e]] = D[v] + Cost[e], To[e]};
				push_heap(H, H+k);
			}
		}
		
		
		if (D[t]==inf) return false;
		rep(v,0,n) for (int e=E[v]; e>0; e=Nxt[e]) Cost[e] -= D[To[e]] - D[v];
		dist+=D[t];
		return true;
	}
	
	T1 dfs(int a, int t, T1 cap) {
		if (a==t or cap==0) return cap;
		
		T1 ret = 0;
		Vst[a] = cnt;
		for (int e=E[a]; e>0 and ret<cap; e=Nxt[e]) 
		{
			if (Cost[e]==0 and Vst[To[e]]<cnt) 
			{
				
				T1 f = dfs(To[e], t, min(cap-ret,Cap[e]));
				
				Cap[e]-=f; 
				Cap[e^1] +=f;
				ret+=f;
			}
		}
		return ret;
	}
	
	pair <T1, T2> flow (int s, int t) {
		T1 cap = 0;
		T2 cost = 0, dist = 0;
		for (;dijkstra(s, t, dist); cnt++) {
			
			T1 df = dfs(s,t,inf);
			cost += df * dist;
			cap += df;
			
		}
		return {cap,cost};
	}
};
