/**
 * Author: Andrzej Turko
 * Date: 2020-20-20
 * License: CC0
 * Description:
 * Time:
 */
namespace dinic {
	typedef int T;
	struct edge {
		T cap, flow;
		int b;
	};
	const int N=2600;
	const T INF = 1e9;
	vector <edge> e;
	vector <int> graf[N];
	int d[N], ptr[N];
	void init (int n = N) {
		rep(i,0,n) graf[i].clear();
		e.clear();
	}
	void add_edge (int a, int b, T cap1, T cap2 = 0) {
		graf[a].pb((int)e.size());
		graf[b].pb((int)e.size()+1);
		e.pb({cap1,0,b}); 
		e.pb({cap2,0,a});
	}
		
	bool bfs (int s, int t) {
		queue <int> kol;
		rep(i,0,N) d[i] = N;
		d[s]=0;
		kol.push(s);
		while (!kol.empty()) {
			int a = kol.front();
			kol.pop();
			if (a==t) return true;
			for (int v: graf[a]) {
				if (e[v].cap == e[v].flow) continue;
				if (d[e[v].b] > d[a] +1LL ) {
					d[e[v].b] = d[a]+1LL;
					kol.push(e[v].b);
				}
			}
		}
		return false;
	}
	T dfs (int a, int t, T flow) {
		if (a==t || flow==0) return flow;
		for(; ptr[a]<(int)graf[a].size(); ptr[a]++){
			int v= graf[a][ptr[a]];
			if (d[e[v].b]!=d[a]+1) continue;
			T x = dfs(e[v].b, t, min(flow, e[v].cap - e[v].flow));
			if (x==0) continue;
			e[v].flow+=x;
			e[v^1].flow-=x; 
			return x;
		}
		return 0;
	}
	T flow (int s, int t) {
		T x, ret=0;
		while (bfs(s, t)) {
			memset(ptr, 0, sizeof ptr);
			while (x = dfs(s, t, INF)) {
				ret+=x;
			}
		}
		return ret;
	}
};
