/**
 * Author: Andrzej Turko
 * Date: 2020-20-20
 * License: CC0
 * Description:
 * Time:
 */
 
ll cross(P a, P b) { return (ll)a.f * b.s - (ll)a.s * b.f; }
int half(P p) {
	if (p.f > 0 || (p.f == 0 && p.s > 0)) return 0;
	return 1;
}
// start is sum leftmost point / each hull is traversed in ccw order
// sort offsets by CCW angle such that (0, -1) comes last
	sort(begin(offsets), end(offsets), [](P a, P b) {
		if (half(a) != half(b)) return half(a) < half(b);
		return cross(a, b) > 0;
	});
