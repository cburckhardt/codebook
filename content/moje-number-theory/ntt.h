/**
 * Author: Andrzej Turko
 * Date: 2020-20-20
 * License: CC0
 * Description:
 * Time:
 */
const int ep = 23;
const ll mod = 119 * (1<<23) +1;
const ll rt = 31; //primitive root of unity modulo mod (degree 2^23)
const ll inw = 128805723;	//it's inverse

ll pot (ll x, int k) {
	ll ret = 1LL;
	while (k>0) {
		if (k%2) ret = ret * x % mod;
		x = x*x % mod;
		k/=2;
	}
	return ret;
}
//absolute values of coeficients should not exceed mod
namespace ntt {
	void ntt (int q, ll *R, ll *w, ll *res) {
		int n = 1<<q;
		int rob[2][n], l, r, k=0, s=1;
		rep(i,0,n) rob[0][i] = i;
		while ((q--)>0) {
			l=0, r=n/2;
			rep(i,0,n){
				if ((1<<q) & rob[k][i]) rob[k^1][r++] = rob[k][i];
				else rob[k^1][l++] = rob[k][i];
			}
			k^=1;
		}
		rep(i,0,n) res[i] = w[rob[k][i]];
		q=1;
		while(s<n) {
			for (int p=0; p<n; p+=2*s) {
				ll c = 1;
				rep(i,0,s) {
				ll tr = res[p+i+s] * c % mod, tl = res[p+i];
				c = c * R[q] % mod;
				res[p+i] = (tl + tr) % mod;
				res[p+i+s] = (tl - tr + mod) %mod;
				}
			}
			s*=2; q++;
		}
	}
	void mult (vector <ll> &w1, vector <ll> &w2, vector <ll> &res) {
		int q=0, n=1;
		while ((int)w1.size() + (int)w2.size() > n) {n*=2; q++;}
		ll w[n], r1[n], r2[n], rtab[ep+1], inwtab[ep+1], d=pot(n, mod-2);
		rtab[ep] = rt, inwtab[ep] = inw;
		for (int i=ep; i>0; i--) {
			rtab[i-1] = rtab[i] * rtab[i] % mod;
			inwtab[i-1] = inwtab[i] * inwtab[i] % mod;
		}
		rep(i,0,n) {
			if (i<(int)w1.size()) w[i] = (w1[i] + mod) % mod;
			else  w[i] = 0;
		}
		ntt(q, rtab, w, r1);
		rep(i,0,n) {
			if (i<(int)w2.size()) w[i] = (w2[i] + mod) %mod;
			else w[i] = 0;
		}
		ntt(q, rtab, w, r2);
		rep(i,0,n) w[i] = r1[i] * r2[i] %mod;
		ntt(q, inwtab, w, r1);
		res.clear();
		res.resize(n);
		rep(i,0,n) res[i] = r1[i] * d % mod;
	}
}
