/**
 * Author: Andrzej Turko
 * Date: 2020-20-20
 * License: CC0
 * Description:
 * Time:
 */
namespace fft {
	typedef long double T;
	const T pi = acos(-1.0);
	struct C {
		T re, im;
		C (T a, T b) : re(a), im(b) {}
		C (T a): re(a), im(0.0) {}
		C (): re(0.0), im(0.0) {}
	};
	C operator * (C a, C b) {
		return C(a.re * b.re - a.im * b.im, a.re * b.im + a.im * b.re);
	}
	C operator + (C a, C b) {
		return C(a.re + b.re, a.im + b.im);
	}
	C operator - (C a, C b) {
		return C(a.re - b.re, a.im - b.im);
	}
	void fft (int n, T d, C *w, C *res) {
		int rob[2][n], l, r, k=0, s=1, q=0;
		rep(i,0,n) rob[0][i] = i;
		while ((1<<q)<n) q++;
		while ((q--)>0) {
			l=0, r=n/2;
			rep(i,0,n) {
				if ( (1<<q) & rob[k][i]) rob[k^1][r++] = rob[k][i];
				else rob[k^1][l++] = rob[k][i];
			}
			k^=1;
		}
		rep(i,0,n) res[i] = w[rob[k][i]];
		k=0;
		while (s<n) {
			for (int p=0; p<n; p+=2*s) rep(i,0,s) {
				C tr=res[p+i+s]* C(cos(d*pi* T(i)/T(s)),sin(d*pi* T(i)/T(s))),tl=res[p+i];
				res[p+i] = tl + tr;
				res[p+i+s] = tl - tr;
			}
			s*=2;
		}
	}
	void mult (vector <ll> &w1, vector <ll> &w2, vector <ll> &res) {
		int n = 1;
		while ((int)w1.size() + (int)w2.size() > n) n*=2;
		C w[n], r1[n], r2[n];
		rep(i,0,n) {
			if (i < (int)w1.size()) w[i] = C(w1[i]);
			else w[i] = C();
		}
		fft(n, 1.0, w, r1);
		rep(i,0,n) {
			if (i <(int)w2.size()) w[i] = C(w2[i]);
			else w[i] = C();
		}
		fft(n, 1.0, w, r2);
		rep(i,0,n) w[i] = r1[i] * r2[i];
		fft(n, -1.0, w, r1);
		res.clear();
		res.resize(n);
		rep(i,0,n) res[i] = llround( r1[i].re) / ll(n);
	}
}
